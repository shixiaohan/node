#!/bin/bash
#
# vdsm-store-net-config: store network configuration files persistently
#

# ifcfg persistence directories
NET_CONF_DIR='/etc/sysconfig/network-scripts/'
NET_CONF_BACK_DIR='@VDSMLIBDIR@/netconfback'

# Unified persistence directories
RUN_CONF_DIR='@VDSMLIBDIR@/staging/netconf'
PERS_CONF_PATH='@VDSMLIBDIR@/persistence'
PERS_NET_CONF_PATH="$PERS_CONF_PATH/netconf"

ifcfg_persist() {
    # Remove the backed up configuration files thus marking the ones under
    # /etc/sysconfig as "safe".
    rm -rf "$NET_CONF_BACK_DIR"/*
}

unified_persist() {
    if [ -d "$RUN_CONF_DIR" ]; then
        # Atomic directory copy by using the atomicity of overwriting a link
        # (rename syscall).
        TIMESTAMP=$(date +%s%N)
        PERS_CONF_SYMLINK=$PERS_NET_CONF_PATH
        PERS_CONF_DIR_ROOTNAME="$PERS_CONF_SYMLINK."
        PERS_CONF_NEW_DIR="$PERS_CONF_DIR_ROOTNAME$TIMESTAMP"
        PERS_CONF_NEW_SYMLINK="$PERS_CONF_SYMLINK.link.$TIMESTAMP"

        cp -r "$RUN_CONF_DIR" "$PERS_CONF_NEW_DIR"
        ln -s "$PERS_CONF_NEW_DIR" "$PERS_CONF_NEW_SYMLINK"
        mv -fT "$PERS_CONF_NEW_SYMLINK" "$PERS_CONF_SYMLINK"
        find "$PERS_CONF_PATH" -type d -path "$PERS_CONF_DIR_ROOTNAME*" | \
            grep -v "$PERS_CONF_NEW_DIR" | xargs rm -fr
    fi
}


ifcfg_persist
unified_persist
